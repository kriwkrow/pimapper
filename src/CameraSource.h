#pragma once

#include "ofMain.h"
#include "FboSource.h"
#include "ofxOMXVideoGrabber.h"

class CameraSource : public ofx::piMapper::FboSource {
public:
	CameraSource();
	void draw();

private:
	ofxOMXVideoGrabber videoGrabber;
	ofxOMXCameraSettings settings;

	int cameraWidth;
	int cameraHeight;
};
