#include "ofApp.h"

void ofApp::setup(){
	ofBackground(0);
	
	piConfigPath = "/boot/config.txt";
	cameraEnabled = false; // Assume that camera is disabled first
	
	if(ofFile::doesFileExist(piConfigPath, false)){
		ofBuffer buffer = ofBufferFromFile(piConfigPath);
		
		for(auto & line: buffer.getLines()){
        	if(line[0] != '#'){
				if(ofIsStringInString(ofToString(line), "start_x=1")){
					cameraEnabled = true; // Flag that camera is enabled
					break;
				}
			}
    	}
	}
	
	if(cameraEnabled){
		cameraSource = new CameraSource();
		mapper.registerFboSource(*cameraSource);
	}

	mapper.setup();
	
	#ifdef TARGET_RASPBERRY_PI
		ofSetFullscreen(true);
	#endif
}

void ofApp::update(){
	mapper.update();
}

void ofApp::draw(){
	mapper.draw();
}

void ofApp::keyPressed(int key){
	mapper.keyPressed(key);
}

void ofApp::keyReleased(int key){
	mapper.keyReleased(key);
}

void ofApp::mouseDragged(int x, int y, int button){
	mapper.mouseDragged(x, y, button);
}

void ofApp::mousePressed(int x, int y, int button){
	mapper.mousePressed(x, y, button);
}

void ofApp::mouseReleased(int x, int y, int button){
	mapper.mouseReleased(x, y, button);
}
