#!/bin/bash

OFX_DIR="ofx"
PM_DELETE="PIMAPPER_DELETE_SOURCES"

DEFAULT_IMAGES[0]="gene-nsynthesis-image-a.jpg"
DEFAULT_IMAGES[1]="gene-nsynthesis-image-b.jpg"
DEFAULT_IMAGES[2]="gene-nsynthesis-image-c.jpg"
DEFAULT_IMAGES[3]="gene-nsynthesis-image-d.jpg"
DEFAULT_IMAGES[4]="gene-nsynthesis-image-e.jpg"

DEFAULT_VIDEOS[0]="gene-nsynthesis-loop-a.mp4"
DEFAULT_VIDEOS[1]="gene-nsynthesis-loop-b.mp4"
DEFAULT_VIDEOS[2]="gene-nsynthesis-loop-c.mp4"

IMAGES_DIR="images"
VIDEOS_DIR="videos"
SOURCE_BACKUP_DIR="backup"

backup_default_sources () {
    echo "Backing up default sources:"

    if [ -d ${SOURCE_BACKUP_DIR} ]; then
        rm -rf ${SOURCE_BACKUP_DIR}
    fi

    mkdir ${SOURCE_BACKUP_DIR}

    for image in ${DEFAULT_IMAGES[*]}
    do
        echo "Backing up ${image}"
        cp "${IMAGES_DIR}/${image}" "${SOURCE_BACKUP_DIR}/"
    done

    for video in ${DEFAULT_VIDEOS[*]}
    do
        echo "Backing up ${video}"
        cp "${VIDEOS_DIR}/${video}" "${SOURCE_BACKUP_DIR}/"
    done

    echo "Done."
}

restore_default_sources () {
    if [ ! -d ${SOURCE_BACKUP_DIR} ]; then
        echo "Nothing to restore."
        return 1
    fi

    echo "Restoring default sources:"

    for image in ${DEFAULT_IMAGES[*]}
    do
        echo "Restoring ${image}"
        cp -f "${SOURCE_BACKUP_DIR}/${image}" "${IMAGES_DIR}/"
    done

    for video in ${DEFAULT_VIDEOS[*]}
    do
        echo "Restoring ${video}"
        cp -f "${SOURCE_BACKUP_DIR}/${video}" "${VIDEOS_DIR}/"
    done

    rm -rf "${SOURCE_BACKUP_DIR}"

    echo "Done."
}

cd /home/pi/${OFX_DIR}/apps/myApps/piMapper/bin/data/sources

# Delete all sources (except default) if PM_DELETE file found on USB
if [ -f "/media/usb/${PM_DELETE}" ]; then
    backup_default_sources

    # Remove all sources
    echo "Removing all sources:"
    rm videos/*
    rm images/*
    echo "Done."

    restore_default_sources

    # Remove file flag 
    rm /media/usb/${PM_DELETE}
fi

# Copy valid video sources
cp -f /media/usb*/*.mp4 videos/ 2>/dev/null
cp -f /media/usb*/*.mov videos/ 2>/dev/null
cp -f /media/usb*/*.mkv videos/ 2>/dev/null

chown pi:pi videos/*.mp4 2>/dev/null
chown pi:pi videos/*.mov 2>/dev/null
chown pi:pi videos/*.mkv 2>/dev/null

# Copy valid image source
cp -f /media/usb*/*.jpg images/ 2>/dev/null
cp -f /media/usb*/*.jpeg images/ 2>/dev/null
cp -f /media/usb*/*.png images/ 2>/dev/null

chown pi:pi images/*.jpg 2>/dev/null
chown pi:pi images/*.jpeg 2>/dev/null
chown pi:pi images/*.png 2>/dev/null

exit 0
