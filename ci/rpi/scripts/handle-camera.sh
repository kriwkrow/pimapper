#!/bin/bash

PM_CAM_ENABLE="PIMAPPER_ENABLE_CAMERA"
PM_CAM_DISABLE="PIMAPPER_DISABLE_CAMERA"

if [ -f "/media/usb/${PM_CAM_ENABLE}" ]; then
    echo "start_x=1" | tee -a /boot/config.txt > /dev/null
    rm /media/usb/${PM_CAM_ENABLE}
    reboot
fi

if [ -f "/media/usb/${PM_CAM_DISABLE}" ]; then
    grep -v "start_x=1" /boot/config.txt | tee /boot/config.txt > /dev/null
    rm /media/usb/${PM_CAM_DISABLE}
fi

exit 0
