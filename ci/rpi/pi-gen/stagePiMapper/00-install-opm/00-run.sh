#!/bin/bash -e

OFX_DIR="ofx"
OFX_VERSION="68eb8a7333f2d301268a41028ceafdd2f0d7d894"
OFX_JSON_VERSION="55a5825ddf510c991466f9d39d8e1d57b2bc2008"
OFX_OMXPLAYER_VERSION="3c047ed51452d1ab0a3deb60ac57e9dd54bb2938"
OFX_PIMAPPER_VERSION="55b3c165df7cf1fb48356f2bb159a659195548ca"
OFX_OMX_CAMERA_VERSION="7871b9c91a79771076d30c6610c774c9bcd1ac16"

if [ -d ${OFX_DIR} ]; then
	rm -rf ${OFX_DIR}
fi

if [ -d ${ROOTFS_DIR}/home/pi/${OFX_DIR} ]; then
	rm -rf ${ROOTFS_DIR}/home/pi/${OFX_DIR}
fi

if [ -d ${ROOTFS_DIR}/home/pi/addons/ofxJSON ]; then
	rm -rf ${ROOTFS_DIR}/home/pi/addons/ofxJSON
fi

if [ -d ${ROOTFS_DIR}/home/pi/addons/ofxOMXPlayer ]; then
	rm -rf ${ROOTFS_DIR}/home/pi/addons/ofxOMXPlayer
fi

if [ -d ${ROOTFS_DIR}/home/pi/apps/myApps/piMapper ]; then
	rm -rf ${ROOTFS_DIR}/home/pi/apps/myApps/piMapper
fi

mkdir "temp"
curl -L -O "https://github.com/openframeworks/openFrameworks/archive/${OFX_VERSION}.zip"
unzip "${OFX_VERSION}.zip" -d "temp" && rm "${OFX_VERSION}.zip"
mv "temp/$(ls temp | head -1)" "${ROOTFS_DIR}/home/pi/${OFX_DIR}"

curl -L -O "https://github.com/jeffcrouse/ofxJSON/archive/${OFX_JSON_VERSION}.zip"
unzip "${OFX_JSON_VERSION}.zip" -d "temp" && rm "${OFX_JSON_VERSION}.zip"
mv "temp/$(ls temp | head -1)" "${ROOTFS_DIR}/home/pi/${OFX_DIR}/addons/ofxJSON"

curl -L -O "https://github.com/jvcleave/ofxOMXPlayer/archive/${OFX_OMXPLAYER_VERSION}.zip"
unzip "${OFX_OMXPLAYER_VERSION}.zip" -d "temp" && rm "${OFX_OMXPLAYER_VERSION}.zip"
mv "temp/$(ls temp | head -1)" "${ROOTFS_DIR}/home/pi/${OFX_DIR}/addons/ofxOMXPlayer"

curl -L -O "https://github.com/jvcleave/ofxOMXCamera/archive/${OFX_OMX_CAMERA_VERSION}.zip"
unzip "${OFX_OMX_CAMERA_VERSION}.zip" -d "temp" && rm "${OFX_OMX_CAMERA_VERSION}.zip"
mv "temp/$(ls temp | head -1)" "${ROOTFS_DIR}/home/pi/${OFX_DIR}/addons/ofxOMXCamera"

curl -L -O "https://github.com/kr15h/ofxPiMapper/archive/${OFX_PIMAPPER_VERSION}.zip"
unzip "${OFX_PIMAPPER_VERSION}.zip" -d "temp" && rm "${OFX_PIMAPPER_VERSION}.zip"
mv "temp/$(ls temp | head -1)" "${ROOTFS_DIR}/home/pi/${OFX_DIR}/addons/ofxPiMapper"

rm -rf "temp"

# For this we need the latest version always
git clone --depth=1 https://gitlab.com/kriwkrow/pimapper.git \
	"${ROOTFS_DIR}/home/pi/${OFX_DIR}/apps/myApps/piMapper"

# Enter chroot on Raspberry Pi (act as if you were root on Pi)
on_chroot << EOF

# Install USB packages and other fun things
apt-get -y install usbmount dosfstools exfat-fuse exfat-utils
apt-get -y install tree htop

# Make Raspbian Stretch to mount our USB drives
sed -i -r "s/MountFlags=slave/MountFlags=shared/" /lib/systemd/system/systemd-udevd.service

# Change memory split, give more to the GPU
echo "gpu_mem_256=128" >> /boot/config.txt
echo "gpu_mem_512=256" >> /boot/config.txt
echo "gpu_mem_1024=512" >> /boot/config.txt
sed -i -r "s/gpu_mem=[0-9]+//" /boot/config.txt

# Fix ownership and permissions as a result using cp and git clone
chown -R pi:pi /home/pi/${OFX_DIR}
chmod -R 755 /home/pi/${OFX_DIR}

# Install openFrameworks dependencies as root
cd /home/pi/${OFX_DIR}/scripts/linux/debian
sed -i "s/apt-get/apt-get -y/g" ./install_dependencies.sh
./install_dependencies.sh

# Download libs
cd /home/pi/${OFX_DIR}/scripts/linux
./download_libs.sh

# Install addon dependencies
cd /home/pi/${OFX_DIR}/addons/ofxOMXPlayer
./install_depends.sh

# Compile as user pi
su pi
cp -f /home/pi/${OFX_DIR}/apps/myApps/piMapper/ci/rpi/addons.make \
    /home/pi/${OFX_DIR}/apps/myApps/piMapper/
make --jobs=$(nproc) Release -C /home/pi/${OFX_DIR}/apps/myApps/piMapper

# Copy and enable startup script to be used by systemd
cp /home/pi/${OFX_DIR}/apps/myApps/piMapper/ci/rpi/scripts/process-sources.sh /home/pi/
chmod a+x /home/pi/process-sources.sh

# Copy and enable camera handling script to be used by systemd
cp /home/pi/${OFX_DIR}/apps/myApps/piMapper/ci/rpi/scripts/handle-camera.sh /home/pi/
chmod a+x /home/pi/handle-camera.sh

# Configure systemd service
sudo cp /home/pi/${OFX_DIR}/apps/myApps/piMapper/ci/rpi/systemd/pimapper.service /etc/systemd/system/
sudo chmod a+x /etc/systemd/system/pimapper.service
sudo systemctl enable pimapper
sudo systemctl start pimapper
EOF

rm -rf ${OFX_DIR}

echo "ofx install successful"

