# piMapper

[![pipeline status](https://gitlab.com/kriwkrow/pimapper/badges/master/pipeline.svg)](https://gitlab.com/kriwkrow/pimapper/commits/master)

Projection mapping software that runs on the Raspberry Pi.

## Description

The goal of this repository is to provide a Raspbian-Based disk image file that you can easily burn on a SD card and use with your Raspberry Pi.

## Usage

1. Download image file
2. Burn it using [Etcher](https://www.balena.io/etcher/)
3. Boot it
4. Upload your own sources using a USB flash drive

## Video Encoding

Easiest way to achieve success is to use [HandBrake](https://handbrake.fr/) with the following settings. This will produce a `.mkv` file with 16bit FLAC audio with 22.05KHz sampling rate.

```
Preset: Fast 720p30
Summary / Format: MKV File
Video / Framerate: Same as source
Video / Profile: Baseline
Audio / Codec: FLAC 16-bit
Audio / Samplerate: 22.05
```

If you are familiar with [ffmpeg](http://ffmpeg.org/), use the following. It will produce a `.mov` file. PCM audio codec with 22KHz sampling rate is used.

```
ffmpeg -i input-video.mp4 -s 1280x720 -aspect 16:9 \
  -c:v libx264 -profile:v baseline \
  -c:a pcm_s16le -ar 22000 -ac 2 \
  output-video.mov
```

These two settings have shown the best results so far. Audio problems and video playback at the beginning of longer video files was the main issue in most cases. Please open an issue if you have better suggestions.

## License

Copyright (c) 2019 [Krisjanis Rijnieks](https://rijnieks.com)

Contact me for commercial use (or rather any use that is not academic research) (email: krisjanis@rijnieks.com). Free for research use, as long as proper attribution is given and this copyright notice is retained.
